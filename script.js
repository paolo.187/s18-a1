console.log("Hello World");

let trainer = Object();
trainer.name = "Paolo",
trainer.gender = "Male",
trainer.weight = "90Kg"
trainer.greeting = function(){
	console.log("Im your trainer")
}
console.log(typeof(trainer));
console.log(trainer)
console.log(trainer['name']);
console.log(trainer.greeting());

function PokemonMaker (name, level, element){
	this.name = name;
	this.level = level;
	this.element = element;
	this.attack = level * 1.3
	this.health = 100 +(1.5* level);
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		target.health -= this.attack;
		console.log(`${target.name} health is now ${target.health}`)
	}
}


let Pikachu = new PokemonMaker("Pikachu",5,"electric");
let Mu = new PokemonMaker("Mu", 12,"All")
let Charizard = new PokemonMaker("Charizard", 8, "Fire");
console.log(Pikachu);
console.log(Mu);
console.log(Charizard);

Pikachu.tackle(Mu);
Mu.tackle(Charizard);
Charizard.tackle(Pikachu);



